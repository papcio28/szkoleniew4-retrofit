package com.android.szkolenie.szkoleniew4_retrofit;

/**
 * Created by papcio28 on 16.05.2015.
 */
public class Kontakt {

    private Long id;    // Koniecznie Long z duzej litery !
    private String imie;
    private String nazwisko;
    private String adresEmail;
    private String numerTelefonu;

    @Override
    public String toString() {
        return imie + " " + nazwisko + "\n" + adresEmail + " Tel: "+numerTelefonu;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getAdresEmail() {
        return adresEmail;
    }

    public void setAdresEmail(String adresEmail) {
        this.adresEmail = adresEmail;
    }

    public String getNumerTelefonu() {
        return numerTelefonu;
    }

    public void setNumerTelefonu(String numerTelefonu) {
        this.numerTelefonu = numerTelefonu;
    }
}
