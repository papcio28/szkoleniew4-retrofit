package com.android.szkolenie.szkoleniew4_retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by papcio28 on 16.05.2015.
 */
public interface ListaKontaktowAPI {

    // Wykonania synchroniczne (w wątku z którego zostały wywołane)

    @GET("/list")   // Żądanie metodą GET, brak parametrów
    public ListKontaktyResponse getKontakty();

    @POST("/add")   // Żądanie metodą POST, @Body - parametr ma być całym ciałem żądania
    public void addKontakt(@Body Kontakt mKontakt);

    @POST("/remove")    // Żądanie metodą POST, @Query - parametr ma być częścią adresu
    public void deleteKontakt(@Query("id") long mKontaktID);

    // Wywołania asynchroniczne (w osobnym watku)

    @GET("/list")   // Żądanie metodą GET, brak parametrów
    public void getKontakty(Callback<ListKontaktyResponse> mCallback);

    @POST("/add")   // Żądanie metodą POST, @Body - parametr ma być całym ciałem żądania
    public void addKontakt(@Body Kontakt mKontakt, Callback<Void> mCallback);

    @POST("/remove")    // Żądanie metodą POST, @Query - parametr ma być częścią adresu
    public void deleteKontakt(@Query("id") long mKontaktID, Callback<Void> mCallback);

    public static class ListKontaktyResponse {
        private List<Kontakt> items;

        public List<Kontakt> getItems() {
            return items;
        }

        public void setItems(List<Kontakt> items) {
            this.items = items;
        }
    }
}
