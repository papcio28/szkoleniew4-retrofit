package com.android.szkolenie.szkoleniew4_retrofit;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemLongClick;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ListaKontaktowActivity extends ActionBarActivity implements Callback<ListaKontaktowAPI.ListKontaktyResponse> {

    @InjectView(R.id.lista_kontaktow)
    protected ListView mLista;

    private ArrayAdapter<Kontakt> mAdapter;
    private ListaKontaktowAPI mAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_kontaktow);
        ButterKnife.inject(this);

        // RestAdapter buduje implementacje naszych interfejsów dostępu do danych
        RestAdapter mRestAdapter = new RestAdapter.Builder()
                // Endpoint to adres bazowy usługi WebService
                .setEndpoint("https://szkolenie-android-kontakty.appspot.com/_ah/api")
                // LogLevel - poziom logowania do LogCata
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        // Budowa implementacji interfejsu za pomocą RestAdaptera
        mAPI = mRestAdapter.create(ListaKontaktowAPI.class);

        mAdapter = new ArrayAdapter<Kontakt>(this, android.R.layout.simple_list_item_1, new ArrayList<Kontakt>());
        mLista.setAdapter(mAdapter);
    }

    // Usuwamy kontakt z serwera po dluzszym przytrzymaniu
    @OnItemLongClick(R.id.lista_kontaktow)
    public boolean usunKontakt(int position) {
        Kontakt mKontakt = mAdapter.getItem(position);
        mAPI.deleteKontakt(mKontakt.getId(), new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                onResume(); // Odświeżamy listę !
            }

            @Override
            public void failure(RetrofitError error) {
                ListaKontaktowActivity.this.failure(error); // Przekazujemy błąd do juz gotowej metody failure w aktywnosci
            }
        });
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Pobranie danych z serwera
        mAPI.getKontakty(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista_kontaktow, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent mIntent = new Intent(this, CreateKontaktActivity.class);
            startActivity(mIntent);
            return true;
        }
        if(id == R.id.action_download) {
            // Stworzenie zadania pobrania pliku przez DownloadManagera
            DownloadManager mDownloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Request mDownloadRequest =
                    new DownloadManager.Request(Uri.parse(
                            "http://www.javacreed.com/wp-content/uploads/2012/12/Gson-Deserialiser-Example.png"));
            // Ustawienie widoczności powiadomienia, w tym przypadku powiadmo o rozpoczęciu i zakończeniu
            mDownloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            mDownloadRequest.setVisibleInDownloadsUi(true); // Czy ma byc widoczne w systemowej aplikacji Pobrane
            mDownloadRequest.setTitle("Obrazek ze szkolenia").setDescription("Z Google");
            // Zachowaj ten plik w katalogu systemowyn Downloads
            mDownloadRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/lisc.png");

            mDownloadManager.enqueue(mDownloadRequest);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void success(ListaKontaktowAPI.ListKontaktyResponse listKontaktyResponse, Response response) {
        List<Kontakt> mListaKontaktow = listKontaktyResponse.getItems();
        mAdapter.clear();
        mAdapter.addAll(mListaKontaktow);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void failure(RetrofitError error) {
        // Pokazanie komunikatu błędu w postaci dialogu
        AlertDialog.Builder mAlert = new AlertDialog.Builder(this)
                .setTitle("Błąd")
                .setMessage(error.getLocalizedMessage())
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        mAlert.show();
    }
}
