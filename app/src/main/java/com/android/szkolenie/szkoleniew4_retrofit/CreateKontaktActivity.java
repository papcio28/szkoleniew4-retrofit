package com.android.szkolenie.szkoleniew4_retrofit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by papcio28 on 16.05.2015.
 */
public class CreateKontaktActivity extends ActionBarActivity implements Callback<Void> {
    @InjectView(R.id.imie)
    protected EditText mImie;

    @InjectView(R.id.nazwisko)
    protected EditText mNazwisko;

    @InjectView(R.id.email)
    protected EditText mEmail;

    @InjectView(R.id.telefon)
    protected EditText mTelefon;

    private ListaKontaktowAPI mAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_kontakt);
        ButterKnife.inject(this);

        // RestAdapter buduje implementacje naszych interfejsów dostępu do danych
        RestAdapter mRestAdapter = new RestAdapter.Builder()
                // Endpoint to adres bazowy usługi WebService
                .setEndpoint("https://szkolenie-android-kontakty.appspot.com/_ah/api")
                        // LogLevel - poziom logowania do LogCata
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        // Budowa implementacji interfejsu za pomocą RestAdaptera
        mAPI = mRestAdapter.create(ListaKontaktowAPI.class);
    }

    @OnClick(R.id.zapisz)
    public void zapiszClick() {
        // Zadanie do zrobienia !!!!
        // Odczytać dane z formularza
        String imie = mImie.getText().toString(),
                nazwisko = mNazwisko.getText().toString(),
                email = mEmail.getText().toString(),
                telefon = mTelefon.getText().toString();
        // Utworzyć obiekt typu kontakt
        Kontakt mKontakt = new Kontakt();
        mKontakt.setImie(imie);
        mKontakt.setNazwisko(nazwisko);
        mKontakt.setAdresEmail(email);
        mKontakt.setNumerTelefonu(telefon);

        // Wysłać obiekt typu kontakt na serwer za pomocą interfejsu API
        mAPI.addKontakt(mKontakt, this);
    }

    @Override
    public void success(Void aVoid, Response response) {
        finish();   // To nie koniecznie musi być w tym miejscu !
    }

    @Override
    public void failure(RetrofitError error) {
        // Pokazanie komunikatu błędu w postaci dialogu
        AlertDialog.Builder mAlert = new AlertDialog.Builder(this)
                .setTitle("Błąd")
                .setMessage(error.getLocalizedMessage())
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        mAlert.show();
    }
}
